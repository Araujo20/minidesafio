/// <reference types="cypress" />

describe('Desafio Hacker news', () => {
  const term = 'cypress.io'
  
  beforeEach(() => {
    cy.intercept('GET', 'https://hn.algolia.com/api/v1/search?query=redux&page=0&hitsPerPage=100', {fixture: 'empty'})
    .as('empty')
    cy.intercept(`https://hn.algolia.com/api/v1/search?query=${term}&page=0&hitsPerPage=100`)
    .as('stories')
    cy.visit('https://hackernews-seven.vercel.app/')
    cy.wait('@empty')
  })

  it('Buscas na aplicação utilizando o cache', () => {
    const faker = require('faker')
    const randomWord = faker.random.word()
    let count = 0

    cy.intercept(`https://hn.algolia.com/api/v1/search?query=${randomWord}**`, req => {
    count +=1
    req.reply({fixture: 'empty'})
  }).as('random')

  cy.search(randomWord).then(() => {
    expect(count, `network calls to fetch ${randomWord}`).to.equal(1)

    cy.wait('@random')

    cy.search(term)
    cy.wait('@stories')

    cy.search(randomWord).then(() => {
      expect(count, `network calls to fetch ${randomWord}`).to.equal(1)
    })
  })
  })
  })