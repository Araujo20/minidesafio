Cypress.Commands.add('search', (texto) => { 
    cy.get('input')
    .clear()
    .type(texto)
    cy.get('form > button')
    .click()
})
